#include "DHT.h"
#include "DHT_U.h"
#define dht_pin 2     
#define DHTTYPE DHT11

DHT dht(dht_pin, DHTTYPE);

void setup() {

  Serial.begin(9600);
  dht.begin();
}

void loop() {

  float h = dht.readHumidity();
  float t = dht.readTemperature();
 


  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
 delay(10000);
}
